package mx.unam.fciencias.pdm.gui_yoplait;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

public class InfiniteListAdapter extends RecyclerView.Adapter<InfiniteListAdapter.ListEntry> {

    private final List<String> DATASET;
    private final Resources RESOURSES;
    private final MasterListItemClickHandler CLICK_HANDLER;

    public InfiniteListAdapter(Resources res, MasterListItemClickHandler listItemClickHandler){
        DATASET = new LinkedList<>();
        RESOURSES = res;
        CLICK_HANDLER = listItemClickHandler;
    }

    public void addItem(){
        int i = DATASET.size();
        DATASET.add(i, RESOURSES.getString(R.string.infinite_list_entry_message,i+1));
        notifyItemInserted(i);
    }


    @NonNull
    @Override
    public ListEntry onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(
                R.layout.infinite_list_entry, parent, false
        );
        return new ListEntry(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull ListEntry holder, int position) {
        holder.entryTest.setText(DATASET.get(position));
    }

    @Override
    public int getItemCount() {
        return DATASET.size();
    }

     class ListEntry extends  RecyclerView.ViewHolder implements View.OnClickListener {
        private  TextView entryTest;

        ListEntry(TextView entryTv){
            super(entryTv);
            entryTest = entryTv;
            entryTv.setClickable(true);
            entryTv.setFocusable(true);
            entryTv.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            CLICK_HANDLER.onItemClicked(getBindingAdapterPosition(), entryTest.getText().toString(),
                    DATASET.size());
        }
    }

    public interface MasterListItemClickHandler {

        void onItemClicked(int clickItemIndex, String entryText, int masterListSize);
    }
}
