package mx.unam.fciencias.pdm.gui_yoplait;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;

public class MainActivity extends MainMenuActivity {
    private Button lauchSecondActivityBottom;
    private String sharedViewTransitionName;
    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_main);
        lauchSecondActivityBottom =  findViewById(R.id.launch_second_activity);
        lauchSecondActivityBottom.setOnClickListener(this::lauchSecondActivity);
    }

    public void lauchSecondActivity(View button){

        Intent intent = new Intent(this, MainActivity2.class);
        if (sharedViewTransitionName == null){
            sharedViewTransitionName = getString(R.string.shared_bottom_transitionName);
        }
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                 this, lauchSecondActivityBottom, sharedViewTransitionName
        );
        resultLauncher.launch(intent,options);

    }
}
